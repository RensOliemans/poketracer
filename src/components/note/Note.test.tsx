import React, {FC, RefObject} from "react";
import Note, {NoteProps} from "./Note";
import {shallow} from "enzyme";

const focus: RefObject<HTMLInputElement> = React.createRef();

const defaultProps = {
    note: '',
    setNote: jest.fn(),
    saveNote: jest.fn(),
    focusRef: focus,
};

const note: FC<NoteProps> = ({
    note,
    setNote,
    saveNote,
    focusRef,
}) => (
    <Note
        note={note}
        setNote={setNote}
        saveNote={saveNote}
        focusRef={focusRef}
    />);

describe("Note", () => {
    it("should exist", () => {
        // Arrange
        const sut = note(defaultProps)!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        expect(wrapper).toHaveLength(1);
    });

    it("should call setNote on input change", () => {
        // Arrange
        const setNote = jest.fn();
        const value = 'something';
        const sut = note({ ...defaultProps, setNote })!!;

        // Act
        const wrapper = shallow(sut);
        wrapper.find('input').simulate('change', { target: { value }} );

        // Assert
        expect(setNote).toHaveBeenCalledTimes(1);
        expect(setNote).toHaveBeenCalledWith(value);
    });

    it("should call saveNote on input submit", () => {
        // Arrange
        const saveNote = jest.fn();
        const sut = note({ ...defaultProps, saveNote })!!;

        // Act
        const wrapper = shallow(sut);
        const preventDefault = jest.fn();
        wrapper.setProps({ note: 'test' });
        wrapper.find('form').simulate('submit', { preventDefault } );

        // Assert
        expect(saveNote).toHaveBeenCalledTimes(1);
        expect(saveNote).toHaveBeenCalledWith('test');
    });
});