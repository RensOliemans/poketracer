import React, {FC, RefObject, useEffect} from "react";
import './note.css';

export type NoteProps = {
  note: string;
  setNote: (note: string) => void;
  saveNote: (note: string) => void;
  focusRef: RefObject<HTMLInputElement>;
}

const Note: FC<NoteProps> = ({note, setNote, saveNote, focusRef}) => {

  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    saveNote(note);
    setNote('')
  }

  useEffect(() => {
    if (note.startsWith('`'))
      setNote(note.substring(1))
  }, [note, setNote])

  return (
    <div className="note-form">
      <form onSubmit={submit}>
        <label>
          Note (` or n to focus)
          <input
            size={25}
            placeholder={"` to focus, enter to save note"}
            type={"text"}
            value={note}
            onChange={e => setNote(e.target.value)}
            required={true}
            ref={focusRef}
          />
        </label>
      </form>
    </div>
  )
};

export default Note;
