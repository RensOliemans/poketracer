import GamePicker from "../gamepicker/GamePicker";
import Info from "../info/Info";
import {getApplicationKeyMap, HotKeys} from "react-hotkeys";
import React, {useCallback, useEffect, useRef, useState} from "react";
import {categories} from "../../constants/platinum";
import {Block} from "../../data/block";
import Link from "../../data/link";
import {Entrance, KeyName} from "../../constants/locations";
import {addLink, addLinks, db} from "../../data/db";
import Tracker from "../tracker/Tracker";
import ImportExport from "../importexport/ImportExport";
import {downloadFile} from "../../util/downloadFile";

const supportedGames = [
  new KeyName("platinum", "Platinum"),
];
const defaultGame = supportedGames[0].key;
const defaultCategory = categories[1];

const Main = () => {
  const [game, setGame] = useState(defaultGame);
  const [links, setLinks] = useState<Link[]>([]);
  const [entrance, setEntrance] = useState<Entrance | null>(null);
  const focusRef = useRef<HTMLInputElement>(null);

  const getAllLinks = async () => {
    return db.links.toArray();
  }

  const getLinks = useCallback(async () => {
    const links = getAllLinks();
    getAllLinks().then(setLinks);
    return links;
  }, [setLinks]);

  useEffect(() => {
    getLinks().catch(console.error);
  }, [getLinks]);

  const removeAllLinks = async () => {
    db.links.toCollection().delete()
      .then(() => setLinks([]));
  }

  const addLinksToDb = async (links: Link[]) => {
    addLinks(links).then(getLinks);
  }

  const clearEntrance = () => {
    setEntrance(null);
  }

  const changeEntrance = (newEntrance: Entrance | null, block: Block | null, note: string | null) => {
    if (!entrance) {
      if (!newEntrance)
        return;

      setEntrance(newEntrance);
      return;
    }

    if (!newEntrance) {
      if (!block && note) {
        addLink(entrance.key, null, false, Block.NONE, note).then(getLinks);
        clearEntrance();
        return;
      } else if (block) {
        addLink(entrance.key, null, false, block, '').then(getLinks);
        clearEntrance();
        return;
      } else {
        clearEntrance();
        return;
      }
    }

    if (entrance === newEntrance)
      return;

    addLink(entrance.key, newEntrance.key, false, Block.NONE, '').then(getLinks);
    clearEntrance();
  };

  const cancel = () => changeEntrance(null, null, null);
  const dead_end = () => changeEntrance(null, Block.DEAD_END, null);
  const cut = () => changeEntrance(null, Block.CUT, null);
  const rock_smash = () => changeEntrance(null, Block.ROCK_SMASH, null);
  const strength = () => changeEntrance(null, Block.STRENGTH, null);
  const surf = () => changeEntrance(null, Block.SURF, null);
  const waterfall = () => changeEntrance(null, Block.WATERFALL, null);
  const rock_climb = () => changeEntrance(null, Block.ROCK_CLIMB, null);
  const trainer = () => changeEntrance(null, Block.TRAINER, null);
  const event = () => changeEntrance(null, Block.EVENT, null);
  const note = () => (focusRef.current && focusRef.current.focus());

  const handlers = {
    CANCEL: cancel, DEAD_END: dead_end, CUT: cut, ROCK_SMASH: rock_smash,
    STRENGTH: strength, SURF: surf, WATERFALL: waterfall, ROCK_CLIMB: rock_climb,
    TRAINER: trainer, EVENT: event, NOTE: note,
  };

  return (
    <HotKeys handlers={handlers} allowChanges={true}>
      <header className="app-header">
        <GamePicker
          supportedGames={supportedGames}
          setGame={setGame}
        />
        <Info
          reset={removeAllLinks}
          getKeyMap={getApplicationKeyMap}
          handlers={handlers}
        />
        <ImportExport
          removeAllLinks={removeAllLinks}
          getAllLinks={getLinks}
          addLinks={addLinksToDb}
          downloadData={downloadFile}
        />
      </header>
      <main>
        <Tracker
          game={game}
          categories={categories}
          defaultCategory={defaultCategory}
          links={links}
          entrance={entrance}
          setEntrance={changeEntrance}
          focusRef={focusRef}
        />
      </main>
  </HotKeys>
)};

export default Main;
