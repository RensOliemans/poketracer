import Info, {InfoProps} from "./Info";
import {shallow} from "enzyme";
import InfoModal from "./InfoModal";
import {FC} from "react";
import ConfirmModal from "../confirmmodal/ConfirmModal";

const defaultProps = {
  reset: jest.fn(),
  getKeyMap: jest.fn(),
  handlers: {},
};

const info: FC<InfoProps> = ({ reset, getKeyMap, handlers }) => (
  <Info reset={reset} getKeyMap={getKeyMap} handlers={handlers} />
);

describe('Info', () => {
  describe('InfoModal', () => {
    it('should have InfoModal', () => {
      // Arrange
      const sut = info(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const infoModal = wrapper.find(InfoModal);
      expect(infoModal).toHaveLength(1);
      expect(infoModal.prop('isOpen')).toBe(false);
    });

    it('should open InfoModal on click', () => {
      // Arrange
      const sut = info(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find('img').simulate('click')

      // Assert
      const infoModal = wrapper.find(InfoModal);
      expect(infoModal).toHaveLength(1);
      expect(infoModal.prop('isOpen')).toBe(true);
    });
  });

  describe('ResetModal', () => {
    it('should have ResetModal', () => {
      // Arrange
      const sut = info(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const resetModal = wrapper.find(InfoModal);
      expect(resetModal).toHaveLength(1);
      expect(resetModal.prop('isOpen')).toBe(false);
    });

    it('should open ConfirmModal on click', () => {
      // Arrange
      const sut = info(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find('button').simulate('click')

      // Assert
      const confirmModal = wrapper.find(ConfirmModal);
      expect(confirmModal).toHaveLength(1);
      expect(confirmModal.prop('isOpen')).toBe(true);
    });

    it("should pass correct props function to ConfirmModal", () => {
      // Arrange
      const reset = jest.fn();
      const sut = info({
        ...defaultProps, reset
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find('button').simulate('click')

      // Assert
      const confirmModal = wrapper.find(ConfirmModal);
      expect(confirmModal.prop('title')).toEqual('Reset All Links')
      expect(confirmModal.prop('onConfirm')).toBe(reset);
    });
  });

  describe('Shortcuts', () => {

    const defaultKeyMap = {
      CANCEL: { sequences: ['c'] },
      DEAD_END: { sequences: ['c'] }
    }

    it('should have as much shortcuts as there are keys in the keyMap', () => {
      const getKeyMap = jest.fn();
      getKeyMap.mockReturnValueOnce(defaultKeyMap);
      const sut = info({
        ...defaultProps, getKeyMap,
      })!!;

      const wrapper = shallow(sut);

      const shortcut = wrapper.findWhere(x => x.hasClass('shortcut'));
      expect(shortcut).toHaveLength(2);
    });
  })

})