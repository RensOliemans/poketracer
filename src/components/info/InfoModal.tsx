import {FC} from "react";
import Modal from '@bdenzer/react-modal';

type InfoModalProps = {
  isOpen: boolean,
  setIsOpen: (flag: boolean) => void,
}

const InfoModal: FC<InfoModalProps> = ({isOpen, setIsOpen}) => {

  const closeModal = () => {
    setIsOpen(false);
  }

  const modalStyle = {
    animationTime: 100,
    modalHeader: {
      backgroundColor: '#282c34',
    },
    modalTitle: {
      color: 'white'
    },
    closeButtonText: {
      color: 'white'
    },
    hoveredButtonText: {
      fontWeight: 'bold'
    },
    modalBody: {
      color: 'black',
    },
  };

  return (
    <Modal
      shouldShowModal={isOpen}
      closeModal={closeModal}
      title="Pokétracer Info"
      customStyle={modalStyle}
    >
      <h3>How To</h3>
      <ul className="shortcut-explanation">
        <li>Click on a category to see its entrances.</li>
        <li>Click on 2 entrances to link them.</li>
        <li>
          Click on an entrance and press one of the shortcuts to mark it as blocked
          (f.e., press <code>d</code> for a Dead End, <code>s</code> for Strength-lock).
        </li>
        <li>Ctrl+Click on an entrance to jump to its destination.</li>
        <li>Shift+Click on 2 entrances to see the fastest route between them.</li>
      </ul>


      <h3>About</h3>
      <p>
        Credits for the randomization to <a href="https://www.youtube.com/c/PointCrow">PointCrow</a>.
        See <a href="https://www.reddit.com/r/pokemon/comments/qel5h4/">here</a> for the Reddit
        post explaining how it works (for Emerald).
        Images come from Bulbapedia, location compilation comes partly from&nbsp;
        <a href="https://www.reddit.com/r/pokemon/comments/qel5h4/comment/hhupues/">twiddlebit</a>.
      </p>

      <h3>Source</h3>
      <p>
        This project is completely open source, click on the GitLab icon below to see its source code.
        You can also add issues there for bugs/new features.
        <br/>
        <a href="https://gitlab.com/RensOliemans/website">
          <img className="source" src={`${process.env.PUBLIC_URL}/gitlab-icon-rgb.svg`} alt="GitLab icon" width="40" height="40" />
        </a>
      </p>
    </Modal>
  )
}

export default InfoModal;
