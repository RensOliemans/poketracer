import {FC, useState} from "react";
import InfoModal from "./InfoModal";
import './Info.css';
import ConfirmModal from "../confirmmodal/ConfirmModal";
import {ApplicationKeyMap} from "react-hotkeys";

export type InfoProps = {
  reset: () => void,
  getKeyMap: () => ApplicationKeyMap,
  handlers: any,
}


const Info: FC<InfoProps> = ({reset, getKeyMap, handlers}) => {
  const [showInfoModal, setShowInfoModal] = useState(false);
  const [showResetModal, setShowResetModal] = useState(false);

  const capitalize =  (s: string): string => {
    s = s.replace('_', ' ');
    return s.toLowerCase().replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
  };

  const execute = (seq: string) => {
    const func = handlers[seq];
    func();
  }

  const createDivForKeyMap = (key: string) => {
    const { sequences } = keyMap[key];
    const name = key.toLowerCase();
    const seq = sequences.map(s => s.sequence);
    return (
      <div onClick={() => execute(key)} key={key} className={"shortcut " + name.toLowerCase()}>{capitalize(name)}: {seq.join(', ')}</div>
    );
  };

  const keyMap = getKeyMap();
  return (
    <div className="header-item info">
      <div className="modal-icon">
        <img
          src={`${process.env.PUBLIC_URL}/info_white_24dp.svg`}
          alt="info"
          onClick={() => setShowInfoModal(true)}
        />
      </div>
      <div>
        <button onClick={() => setShowResetModal(true)}>
          Clear
        </button>
      </div>
      {keyMap && Object.keys(keyMap).map(k => createDivForKeyMap(k))}
      <div className="modal">
        <InfoModal
          isOpen={showInfoModal}
          setIsOpen={setShowInfoModal}
        />
      </div>

      <ConfirmModal
        isOpen={showResetModal}
        setIsOpen={setShowResetModal}
        onConfirm={reset}
        title={"Reset All Links"}
        bodyText={"Are you sure you wish to reset? This cannot be undone."}
      />
    </div>
  )
}

export default Info;
