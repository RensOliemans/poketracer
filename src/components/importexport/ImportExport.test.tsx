import {FC} from "react";
import ImportExport, {ImportExportProps} from "./ImportExport";
import {mount, shallow} from "enzyme";
import ConfirmModal from "../confirmmodal/ConfirmModal";
import Link from "../../data/link";
import {Block} from "../../data/block";
import {act} from "@testing-library/react";

const defaultProps = {
  removeAllLinks: jest.fn(),
  getAllLinks: jest.fn(),
  addLinks: jest.fn(),
  downloadData: jest.fn(),
};

const importExport: FC<ImportExportProps> = ({
  removeAllLinks, getAllLinks, addLinks, downloadData,
}) => {
  return (
    <ImportExport
      removeAllLinks={removeAllLinks}
      getAllLinks={getAllLinks}
      addLinks={addLinks}
      downloadData={downloadData}
    />
)};

const sut = (props: ImportExportProps = defaultProps) => {
  return importExport(props)!!;
}

describe("ImportExport", () => {
  it("should exist", () => {
    // Act
    const wrapper = shallow(sut());

    // Assert
    expect(wrapper).toHaveLength(1);
  });

  describe("Layout", () => {
    it("should have an import input", () => {
      // Act
      const wrapper = shallow(sut());

      // Assert
      const input = wrapper.find('input');
      expect(input).toHaveLength(1);
      expect(input.prop('type')).toEqual('file');
      expect(input.prop('accept')).toEqual('.json');
      expect(wrapper.find('label').text()).toEqual('Import');
    });

    it("should have an export button", () => {
      // Act
      const wrapper = shallow(sut());

      // Assert
      const exportBtn = wrapper.find('button');
      expect(exportBtn).toHaveLength(1);
      expect(exportBtn.text()).toEqual('Export');
    });
  });

  describe("Import functionality", () => {
    const defaultText = (links: Link[] = []) => {
      return new Promise<string>((resolve) => resolve(`${JSON.stringify(links)}`));
    };
    const createFile = (name = 'filename', text = defaultText) => {
      return { name, text };
    };

    it("should not open confirmResetModal before importing", () => {
      // Act
      const wrapper = shallow(sut());

      // Assert
      expect(wrapper.find(ConfirmModal)).toHaveLength(1);
      expect(wrapper.find(ConfirmModal).prop('isOpen')).toEqual(false);
    });

    it("should open confirmResetModal when importing", () => {
      // Arrange
      const file = createFile();

      // Act
      const wrapper = shallow(sut());
      wrapper.find('input').simulate('change', { target: { files: [file] }});

      // Assert
      expect(wrapper.find(ConfirmModal)).toHaveLength(1);
      expect(wrapper.find(ConfirmModal).prop('isOpen')).toEqual(true);
    });

    it("should not call removeAllLinks before importing", () => {
      // Arrange
      const file = createFile();
      const removeAllLinks = jest.fn(() => Promise.resolve());
      const props = { ...defaultProps, removeAllLinks };

      // Act
      act(() => {
        const wrapper = mount(sut(props));
        wrapper.find('input').simulate('change', { target: { files: [file] }});
      });

      // Assert
      expect(removeAllLinks).toHaveBeenCalledTimes(0);
    });

    it("should not call removeAllLinks when importing 0 links", () => {
      // Arrange
      const file = createFile();
      const removeAllLinks = jest.fn(() => Promise.resolve());
      const props = { ...defaultProps, removeAllLinks };

      // Act
      act(() => {
        const wrapper = mount(sut(props));
        wrapper.find('input').simulate('change', { target: { files: [file] }});
        wrapper.find(ConfirmModal).find('button').first().simulate('click');
      });

      // Assert
      expect(removeAllLinks).toHaveBeenCalledTimes(0);
    });

    it("should call removeAllLinks when importing links", () => {
      // Arrange
      const links = [
        new Link('ent1', 'ent2', false, Block.NONE, ''),
      ];
      const file = createFile(undefined, () => defaultText(links));
      const removeAllLinks = jest.fn(() => Promise.resolve());
      const props = { ...defaultProps, removeAllLinks };

      // Act
      act(() => {
        const wrapper = mount(sut(props));
        wrapper.find('input').simulate('change', { target: { files: [file] }});
        file.text().then(() => wrapper.find(ConfirmModal).find('button').first().simulate('click'));
      });

      // Assert
      return file.text().then(() => expect(removeAllLinks).toHaveBeenCalledTimes(1));
    });

    it("should call addLinks after removeAllLinks when importing", () => {
      // Arrange
      const links = [
        new Link('ent1', 'ent2', false, Block.NONE, ''),
        new Link('ent3', 'ent4', false, Block.NONE, ''),
        new Link('ent5', null, false, Block.DEAD_END, ''),
        new Link('ent6', null, false, Block.SURF, ''),
        new Link('ent7', null, false, Block.NONE, 'note'),
      ];
      const file = createFile(undefined, () => defaultText(links));
      const addLinks = jest.fn(() => Promise.resolve());
      const removeAllLinks = jest.fn(() => Promise.resolve());
      const props = { ...defaultProps, addLinks, removeAllLinks };

      // Act
      act(() => {
        const wrapper = mount(sut(props));
        wrapper.find('input').simulate('change', { target: { files: [file] }});
        file.text().then(() => wrapper.find(ConfirmModal).find('button').first().simulate('click'));
      });

      // Assert
      return file.text().then(() => {
        expect(addLinks).toHaveBeenCalledTimes(0);
        removeAllLinks().then(() => {
          expect(addLinks).toHaveBeenCalledTimes(1);
          expect(addLinks).toHaveBeenCalledWith(links);
        });
      });
    });
  });

  describe("Export functionality", () => {
    it("should not export before we call export", () => {
      // Arrange
      const downloadFile = jest.fn();
      shallow(sut({ ...defaultProps, downloadData: downloadFile }));

      // Assert
      expect(downloadFile).toHaveBeenCalledTimes(0);
    });

    it("should export with all known links", () => {
      // Arrange
      const links = [
        new Link('ent1', 'ent2', false, Block.NONE, ''),
        new Link('ent3', 'ent4', false, Block.NONE, ''),
        new Link('ent5', null, false, Block.DEAD_END, ''),
        new Link('ent6', null, false, Block.SURF, ''),
        new Link('ent7', null, false, Block.NONE, 'note'),
      ];
      const downloadData = jest.fn();
      const getAllLinks = jest.fn();
      getAllLinks.mockResolvedValue(links);
      const wrapper = mount(sut({ ...defaultProps, downloadData, getAllLinks }));

      // Act
      wrapper.find('button').first().simulate('click');

      // Assert
      return getAllLinks().then(() => {
        expect(downloadData).toHaveBeenCalledTimes(1);
        expect(downloadData).toHaveBeenCalledWith([JSON.stringify(links)], "poketracer-export.json")
      });
    });
  });
});
