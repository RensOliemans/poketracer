import './importexport.css';
import React, {FC, useState} from "react";
import Link from "../../data/link";
import ConfirmModal from "../confirmmodal/ConfirmModal";

export type ImportExportProps = {
  removeAllLinks: () => Promise<void>;
  getAllLinks: () => Promise<Link[]>;
  addLinks: (links: Link[]) => Promise<void>;
  downloadData: (data: string[], filename: string) => void;
}

const ImportExport: FC<ImportExportProps> = ({
  removeAllLinks, getAllLinks, addLinks, downloadData,
}) => {
  const [confirmModalIsOpen, setConfirmModalIsOpen] = useState(false);
  const [filename, setFilename] = useState('');
  const [amountOfLinks, setAmountOfLinks] = useState(0);
  const [linksToImport, setLinksToImport] = useState<Link[]>([]);
  const [bodyText, setBodyText] = useState('');

  const uploadFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files!![0];
    setFilename(file.name);
    file.text().then(readFile);
    setConfirmModalIsOpen(true);
  }

  const readFile = (data: string) => {
    const links: Link[] = JSON.parse(data).map((obj: Link) => getExplicitLink(obj));
    setAmountOfLinks(links.length);
    setLinksToImport(links);
    fillBody();
  }

  const getExplicitLink = (obj: Link) => {
    return new Link(obj.entrance, obj.destination, obj.one_way, obj.block, obj.note);
  }

  const confirmImport = () => {
    if (linksToImport.length) {
      setBodyText(`Importing ${amountOfLinks} links.`);
      removeAllLinks()
        .then(() => addLinks(linksToImport))
        .then(() => setConfirmModalIsOpen(false));
    } else {
      setBodyText('No links found in file. Try uploading another file');
    }
  }

  const dataExport = async () => {
    getAllLinks().then(exportLinks);
  };

  const exportLinks = (links: Link[]) => {
    const linkData = [JSON.stringify(links)]
    downloadData(linkData, "poketracer-export.json")
  }

  const fillBody = () => {
    setBodyText(`Are you sure you wish to import all ${amountOfLinks} links of ${filename}? ` +
        `This will remove all currently existing links in the database.`);
  }

  return (
    <div className={"importExport"}>
      <label>
        Import
        <input onChange={uploadFile} type={"file"} name={"import"} accept={".json"} />
      </label>
      <button onClick={dataExport}>Export</button>
      <ConfirmModal
        isOpen={confirmModalIsOpen}
        setIsOpen={setConfirmModalIsOpen}
        onConfirm={confirmImport}
        title={`Confirm Import of ${amountOfLinks} links`}
        bodyText={bodyText}
      />
    </div>
  )
}

export default ImportExport;
