import {FC} from "react";
import Modal from '@bdenzer/react-modal';

export type ConfirmModalProps = {
  isOpen: boolean,
  setIsOpen: (flag: boolean) => void,
  onConfirm: () => void,
  title: string,
  bodyText: string,
}

const ConfirmModal: FC<ConfirmModalProps> = ({
  isOpen,
  setIsOpen,
  onConfirm,
  title,
  bodyText,
}) => {

  const closeModal = () => {
    setIsOpen(false);
  }

  const confirm = () => {
    onConfirm();
    closeModal();
  }

  const modalStyle = {
    animationTime: 100,
    modalHeader: {
      backgroundColor: '#282c34',
    },
    modalTitle: {
      color: 'white'
    },
    closeButtonText: {
      color: 'white'
    },
    hoveredButtonText: {
      fontWeight: 'bold'
    },
    modalBody: {
      color: 'black',
    }
  };

  return (
    <Modal
      shouldShowModal={isOpen}
      closeModal={closeModal}
      title={title}
      customStyle={modalStyle}
    >
      {bodyText}
      <br />
      <button
        onClick={confirm}
      >
        Yes
      </button>
      &nbsp;
      <button
        onClick={closeModal}
      >
        No
      </button>
    </Modal>
  )
}

export default ConfirmModal;
