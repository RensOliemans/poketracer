import ConfirmModal, {ConfirmModalProps} from "./ConfirmModal";
import {shallow} from "enzyme";
import Modal from "@bdenzer/react-modal";
import {FC} from "react";

const defaultProps = {
  isOpen: false,
  setIsOpen: jest.fn(),
  onConfirm: jest.fn(),
  title: '',
  bodyText: '',
};

const confirmModal: FC<ConfirmModalProps> = ({ isOpen, setIsOpen, onConfirm, title, bodyText }) => {
  return <ConfirmModal
    isOpen={isOpen}
    setIsOpen={setIsOpen}
    onConfirm={onConfirm}
    title={title}
    bodyText={bodyText}
  />;
}

describe('ConfirmModal', () => {
  it('should exist', () => {
    // Arrange
    const sut = confirmModal(defaultProps)!!;

    // Act
    const wrapper = shallow(sut);

    // Assert
    expect(wrapper).toHaveLength(1);
  });

  it('should be visible', () => {
    // Arrange
    const setIsOpen = jest.fn();
    const sut = confirmModal({ ...defaultProps, isOpen: true, setIsOpen })!!;

    // Act
    const wrapper = shallow(sut);

    // Assert
    expect(wrapper).toHaveLength(1);
    expect(wrapper.find(Modal).prop('shouldShowModal')).toEqual(true)
    expect(setIsOpen).toBeCalledTimes(0);
  });

  it('should call confirm on first button click', () => {
    // Arrange
    const confirm = jest.fn();
    const sut = confirmModal({ ...defaultProps, isOpen: true, onConfirm: confirm })!!;

    // Act
    const wrapper = shallow(sut);
    wrapper.find('button').first().simulate('click');

    // Assert
    expect(confirm).toHaveBeenCalledTimes(1);
  });

  it('should not call confirm on second button click', () => {
    // Arrange
    const confirm = jest.fn();
    const sut = confirmModal({ ...defaultProps, isOpen: true, onConfirm: confirm })!!;

    // Act
    const wrapper = shallow(sut);
    wrapper.find('button').last().simulate('click');

    // Assert
    expect(confirm).toHaveBeenCalledTimes(0);
  });

  it('should close modal on confirm', () => {
    // Arrange
    const confirm = jest.fn();
    const setIsOpen = jest.fn();
    const sut = confirmModal({ ...defaultProps, onConfirm: confirm, setIsOpen })!!;

    // Act
    const wrapper = shallow(sut);
    wrapper.find('button').first().simulate('click');

    // Assert
    expect(setIsOpen).toBeCalledWith(false);
    expect(setIsOpen).toBeCalledTimes(1);
  })
});