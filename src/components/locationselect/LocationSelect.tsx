import React, {FC, RefObject, useState} from "react";
import Categories from "../categories/Categories";
import {Category, Entrance} from "../../constants/locations";
import Entrances from "../entrances/Entrances";
import './LocationSelect.css';
import Link from "../../data/link";
import {entrancesOfCategory, getEntranceFromKey, other} from "../../util";
import {Block} from "../../data/block";

export type LocationSelectProps = {
  categories: Category[];
  category: Category;
  setCategory: (category: Category) => void;
  entrance: Entrance | null;
  setEntrance: (entrance: Entrance | null, block: Block | null, note: string | null) => void;
  setRoute: (entrance: Entrance) => void;
  links: Link[];
  focusRef: RefObject<HTMLInputElement>;
}

const LocationSelect: FC<LocationSelectProps> = ({
  categories,
  category,
  setCategory,
  entrance,
  setEntrance,
  setRoute,
  links,
  focusRef,
}) => {
  const [entrancesToHighlight, setEntrancesToHighlight] = useState<string[]>(['']);
  const [categoriesToHighlight, setCategoriesToHighlight] = useState(['']);

  const entrances = entrancesOfCategory(categories, category);

  const saveEntrance = (newEntrance: Entrance | null, block: Block | null, note: string | null) => {
    setEntrance(newEntrance, block, note);
  }

  const onEnterCategory = (cat: Category) => {
    if (cat === category)
      return;

    const entrances = links.map(l => {
      const entrance = getEntranceFromKey(categories, l.entrance);
      const destination = l.destination && getEntranceFromKey(categories, l.destination);
      if (entrance && entrance.location.category === cat && l.destination)
        return l.destination;
      else if (destination && destination.location.category === cat)
        return l.entrance;
      return '';
    }).filter(e => e !== '');

    setEntrancesToHighlight(entrances);
  };

  const onEnterEntrance = (ent: Entrance) => {
    const hoveredLinks = links.filter(l => l.entrance === ent.key || l.destination === ent.key)
    if (hoveredLinks.length === 0)
      return

    const dest = other(hoveredLinks[0], ent.key);
    const category = getEntranceFromKey(categories, dest);
    if (category)
      setCategoriesToHighlight([category.location.category.key]);
  };

  const getEntrance = (key: string) => {
    return getEntranceFromKey(categories, key);
  };

  return (
    <>
      <div className="location-select">
        <Categories
          category={category}
          categories={categories}
          categoriesToHighlight={categoriesToHighlight}
          setCategory={setCategory}
          links={links}
          onEnter={onEnterCategory}
          onLeave={() => setEntrancesToHighlight([])}
        />
        <Entrances
          entrance={entrance}
          entrances={entrances}
          setEntrance={saveEntrance}
          setCategory={setCategory}
          setRoute={setRoute}
          links={links}
          entrancesToHighlight={entrancesToHighlight}
          onEnter={onEnterEntrance}
          onLeave={() => setCategoriesToHighlight([])}
          getEntranceFromKey={getEntrance}
          focusRef={focusRef}
        />
      </div>
    </>
  )
}

export default LocationSelect;
