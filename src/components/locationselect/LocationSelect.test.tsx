import React, {FC, RefObject} from "react";
import LocationSelect, {LocationSelectProps} from "./LocationSelect";
import {Category} from "../../constants/locations";
import {mount, shallow} from "enzyme";
import Categories from "../categories/Categories";
import Entrances from "../entrances/Entrances";
import Link from "../../data/link";
import {Block} from "../../data/block";
import {cats} from "../categories/Categories.test";
import ButtonGrid from "../buttongrid/ButtonGrid";
import {entrancesOfCategory} from "../../util";

const focus: RefObject<HTMLInputElement> = React.createRef();
const defaultCategory = new Category('cat', 'category')

const defaultProps = {
    categories: [defaultCategory],
    category: defaultCategory,
    setCategory: jest.fn(),
    entrance: null,
    setEntrance: jest.fn(),
    setRoute: jest.fn(),
    links: [],
    focusRef: focus,
};

const locationSelect: FC<LocationSelectProps> = ({
    categories, category, setCategory, entrance,
    setEntrance, setRoute, links, focusRef,
}) => (
    <LocationSelect
        categories={categories}
        category={category}
        setCategory={setCategory}
        entrance={entrance}
        setEntrance={setEntrance}
        setRoute={setRoute}
        links={links}
        focusRef={focusRef}
    />);

describe("LocationSelect", () => {
    it("should exist", () => {
        // Arrange
        const sut = locationSelect(defaultProps)!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        expect(wrapper).toHaveLength(1);
    });

    describe("Categories", () => {
        it("should exist", () => {
            // Arrange
            const sut = locationSelect(defaultProps)!!;

            // Act
            const wrapper = shallow(sut);

            // Assert
            expect(wrapper.find(Categories)).toHaveLength(1);
        });

        it("should pass correct props", () => {
            // Arrange
            const links = [
                new Link('a1', 'e1', false, Block.NONE, '')
            ];
            const sut = locationSelect({ ...defaultProps, links })!!;

            // Act
            const wrapper = shallow(sut);

            // Assert
            const categories = wrapper.find(Categories);
            expect(categories.prop('category')).toBe(defaultProps.category);
            expect(categories.prop('categories')).toBe(defaultProps.categories);
            expect(categories.prop('setCategory')).toBe(defaultProps.setCategory);
            expect(categories.prop('links')).toBe(links);
        });

        it("should set correct categories to highlight on entrance hover", () => {
            // Arrange
            const links = [
                new Link('e1', 'e2', false, Block.NONE, ''),
                new Link('e3', 'e4', false, Block.NONE, ''),
            ];
            const sut = locationSelect({
                ...defaultProps,
                categories: cats, category: cats[0], links,
            })!!;

            // Act
            const wrapper = mount(sut);
            const buttonGrid = wrapper.find(Entrances).find(ButtonGrid)
            buttonGrid.find('button').first().simulate('mouseenter')

            // Assert
            expect(wrapper.find(Categories).prop('categoriesToHighlight')).toEqual(['cat2'])
        });
    });

    describe("Entrances", () => {
        it("should exist", () => {
            // Arrange
            const sut = locationSelect(defaultProps)!!;

            // Act
            const wrapper = shallow(sut);

            // Assert
            expect(wrapper.find(Entrances)).toHaveLength(1);
        });

        it("should pass correct props", () => {
            // Arrange
            const ent = cats[0].locations[0].entrances[0];
            const links = [
                new Link('a1', 'e1', false, Block.NONE, '')
            ];
            const sut = locationSelect({
                ...defaultProps,
                categories: cats,
                category: cats[0],
                entrance: ent,
                links,
            })!!;

            // Act
            const wrapper = shallow(sut);
            const entrances = wrapper.find(Entrances);

            // Assert
            const expectedEntrances = entrancesOfCategory(cats, cats[0]);
            expect(entrances.prop('entrance')).toBe(ent);
            expect(entrances.prop('entrances')).toEqual(expectedEntrances);
            expect(entrances.prop('setCategory')).toBe(defaultProps.setCategory);
            expect(entrances.prop('setRoute')).toBe(defaultProps.setRoute);
            expect(entrances.prop('links')).toBe(links);
        });

        it("should set correct entrances to highlight on category hover without links", () => {
            // Arrange
            const sut = locationSelect({
                ...defaultProps,
                categories: cats, category: cats[0],
            })!!;

            // Act
            const wrapper = mount(sut);
            const buttonGrid = wrapper.find(Categories).find(ButtonGrid);

            // Assert
            buttonGrid.find('button').first().simulate('mouseenter');
            expect(wrapper.find(Entrances).prop('entrancesToHighlight')).toEqual([''])
        });

        it("should set correct entrances to highlight on selected category hover", () => {
            // Arrange
            const links = [
                new Link('e1', 'e2', false, Block.NONE, ''),
            ];
            const sut = locationSelect({
                ...defaultProps,
                categories: cats, category: cats[0], links,
            })!!;

            // Act
            const wrapper = mount(sut);
            const buttonGrid = wrapper.find(Categories).find(ButtonGrid);

            // Assert
            buttonGrid.find('button').first().simulate('mouseenter');
            expect(wrapper.find(Entrances).prop('entrancesToHighlight')).toEqual([''])
        });

        it("should set correct entrances to highlight on category hover with links", () => {
            // Arrange
            const links = [
                new Link('e1', 'e2', false, Block.NONE, ''),
                new Link('e3', 'e4', false, Block.NONE, ''),
            ];
            const sut = locationSelect({
                ...defaultProps,
                categories: cats, category: cats[0], links,
            })!!;

            // Act
            const wrapper = mount(sut);
            const buttonGrid = wrapper.find(Categories).find(ButtonGrid);

            // Assert
            buttonGrid.find('button').at(1).simulate('mouseenter');
            expect(wrapper.find(Entrances).prop('entrancesToHighlight')).toEqual(['e1', 'e4'])
        });

        it("should set entrance on entrance click", () => {
            // Arrange
            const setEntrance = jest.fn();
            const sut = locationSelect({
                ...defaultProps,
                categories: cats, category: cats[0], setEntrance,
            })!!;

            // Act
            const wrapper = mount(sut);
            wrapper.find(Entrances).find('button').first().simulate('click');

            // Assert
            expect(setEntrance).toHaveBeenCalledTimes(1);
            expect(setEntrance).toHaveBeenCalledWith(cats[0].locations[0].entrances[0], null, null);
        });
    });
});