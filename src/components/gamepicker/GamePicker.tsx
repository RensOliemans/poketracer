import React, {FC} from "react";
import './GamePicker.css';
import {KeyName} from "../../constants/locations";

export type GamePickerProps = {
  supportedGames: KeyName[],
  setGame: (game: string) => void;
}

const GamePicker: FC<GamePickerProps> = ({
  supportedGames,
  setGame,
}) => {
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setGame(e.target.value);
  }

  return (
    <div className="header-item">
      Currently selected game
      <br/>
      <select
        className="select-game"
        onChange={handleChange}
      >
        {supportedGames.map(g =>
          <option
            key={g.key}
            value={g.key}
          >
            {g.name}
          </option>
        )}
      </select>
    </div>
  )
}

export default GamePicker;
