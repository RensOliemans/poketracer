import GamePicker, {GamePickerProps} from "./GamePicker";
import {KeyName} from "../../constants/locations";
import { shallow } from "enzyme";
import {FC} from "react";


const defaultProps = {
  supportedGames: [],
  setGame: jest.fn(),
}

const gamePicker: FC<GamePickerProps> = ({
    supportedGames,
    setGame,
}) => (<GamePicker
    supportedGames={supportedGames}
    setGame={setGame}
  />);


describe("GamePicker", () => {
  it("should show the supported games", () => {
    // Arrange
    const supportedGames = [
      new KeyName("emerald", "Emerald"),
      new KeyName("platinum", "Platinum"),
    ];
    const sut = gamePicker({ ...defaultProps, supportedGames })!!;

    // Act
    const wrapper = shallow(sut)

    // Assert
    const select = wrapper.find("select")
    expect(select).toBeDefined();
    expect(select.find("option").length).toEqual(supportedGames.length);
    supportedGames.forEach(kn => {
      expect(wrapper.find({ value: kn.key }).text()).toEqual(kn.name);
    });
  });

  it("should call setGame on game select", () => {
    // Arrange
    const supportedGames = [
      new KeyName("emerald", "Emerald"),
      new KeyName("platinum", "Platinum"),
    ];
    const setGame = jest.fn();
    const sut = gamePicker({ supportedGames, setGame })!!;

    // Act
    const wrapper = shallow(sut)
    wrapper.find("select").simulate("change", { target:  { value: supportedGames[1].key} })

    // Assert
    expect(setGame).toHaveBeenCalledTimes(1);
    expect(setGame).toHaveBeenCalledWith(supportedGames[1].key);
  })
})