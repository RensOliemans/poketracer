import {Category, Entrance, Location} from "../../constants/locations";
import {FC} from "react";
import Categories, {CategoriesProps} from "./Categories";
import {shallow} from "enzyme";
import Link from "../../data/link";
import {Block} from "../../data/block";
import ButtonGrid from "../buttongrid/ButtonGrid";

const defaultProps = {
    category: new Category('cat', 'Category'),
    categories: [],
    setCategory: jest.fn(),
    categoriesToHighlight: [],
    links: [],
    onEnter: jest.fn(),
    onLeave: jest.fn(),
};

export const cats = [
    new Category('cat1', 'Category 1'),
    new Category('cat2', 'Category 2'),
    new Category('cat3', 'Category 3'),
    new Category('cat4', 'Category 4'),
];
export const locations = [
    new Location('loc1', 'Location 1', cats[0]),
    new Location('loc2', 'Location 2', cats[1]),
    new Location('loc3', 'Location 3', cats[1]),
    new Location('loc4', 'Location 4', cats[2]),
    new Location('loc5', 'Location 5', cats[3]),
];
cats[0].locations = [locations[0]];
cats[1].locations = [locations[1], locations[2]]
cats[2].locations = [locations[3]]
cats[3].locations = [locations[4]];
export const entrances = [
    new Entrance('e1', 'Entrance 1', locations[0]),
    new Entrance('e2', 'Entrance 2', locations[1]),
    new Entrance('e3', 'Entrance 3', locations[2]),
    new Entrance('e4', 'Entrance 4', locations[3]),
    new Entrance('e5', 'Entrance 5', locations[4]),
];
locations[0].entrances = [entrances[0]];
locations[1].entrances = [entrances[1]];
locations[2].entrances = [entrances[2]];
locations[3].entrances = [entrances[3]];
locations[4].entrances = [entrances[4]];

const categories: FC<CategoriesProps> = ({
    category, categories, setCategory, categoriesToHighlight, links, onEnter, onLeave
}) => (
    <Categories category={category}
                categories={categories}
                setCategory={setCategory}
                categoriesToHighlight={categoriesToHighlight}
                links={links}
                onEnter={onEnter}
                onLeave={onLeave}
    />);

describe("Categories", () => {
    it("should exist", () => {
        // Arrange
        const sut = categories(defaultProps)!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        expect(wrapper).toHaveLength(1);
    });

    it("should convert basic links correctly", () => {
        // Arrange
        const links = [
            new Link('e1', 'e2', false, Block.NONE, ''),
            new Link('e3', 'e4', false, Block.NONE, ''),
        ];
        const sut = categories({
            ...defaultProps,
            category: cats[1],
            categories: cats,
            links: links
        })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const expected = ['cat1', 'cat3']
        expect(wrapper.find(ButtonGrid).prop('highlightedKeys')).toEqual(expected);
    });

    it("should pass props on to ButtonGrid", () => {
        // Arrange
        const cats = [
            new Category('cat1', 'Category 1'),
            new Category('cat2', 'Category 2'),
            new Category('cat3', 'Category 3'),
            new Category('cat4', 'Category 4'),
        ];
        const categoriesToHighlight = ['cat1']
        const sut = categories({
            ...defaultProps,
            categories: cats, categoriesToHighlight,
        })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const buttonGrid = wrapper.find(ButtonGrid);
        expect(buttonGrid.prop('keyNamePairs')).toEqual(cats);
        expect(buttonGrid.prop('linkedKeys')).toEqual(categoriesToHighlight);
        expect(buttonGrid.prop('onClick')).toEqual(defaultProps.setCategory);
        expect(buttonGrid.prop('onCtrlClick')).toEqual(defaultProps.setCategory);
        expect(buttonGrid.prop('onShiftClick')).toEqual(defaultProps.setCategory);
        expect(buttonGrid.prop('onEnter')).toEqual(defaultProps.onEnter);
        expect(buttonGrid.prop('onLeave')).toEqual(defaultProps.onLeave);
    });
});
