import {FC} from "react";
import ButtonGrid from "../buttongrid/ButtonGrid";
import {Category} from "../../constants/locations";
import './Categories.css'
import Link from "../../data/link";
import {getEntranceFromKey} from "../../util";


export type CategoriesProps = {
  category: Category,
  categories: Category[];
  setCategory: (newCategory: Category) => void;
  categoriesToHighlight: string[];
  links: Link[];
  onEnter: (category: Category) => void;
  onLeave: () => void;
}

const Categories: FC<CategoriesProps> = ({
  category,
  categories,
  setCategory,
  categoriesToHighlight,
  links,
  onEnter,
  onLeave,
}) => {

  const convertLinks = (links: Link[]) => {
    const highlights: string[] = [];
    links.forEach(l => {
      const entrance = getEntranceFromKey(categories, l.entrance);
      if (entrance && entrance.location.category !== category)
        highlights.push(entrance.location.category.key);
      const destination = getEntranceFromKey(categories, l.destination);
      if (destination && destination.location.category !== category)
        highlights.push(destination.location.category.key);
    })
    return highlights;
  };

  return (
    <div className="categories">
      <h2>Categories</h2>
      <ButtonGrid
        keyNamePairs={categories}
        highlightedKeys={convertLinks(links)}
        linkedKeys={categoriesToHighlight}
        onClick={setCategory}
        onCtrlClick={setCategory}
        onShiftClick={setCategory}
        onEnter={onEnter}
        onLeave={onLeave}
      />
    </div>
  )
}

export default Categories;
