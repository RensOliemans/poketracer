import {Category} from "../../constants/locations";
import {FC} from "react";
import ImageViewer, {ImageViewerProps} from "./ImageViewer";
import {shallow} from "enzyme";

const defaultProps = {
    game: '',
    category: new Category('cat', 'Category'),
};

const imageViewer: FC<ImageViewerProps> = ({ game, category }) => (
    <ImageViewer game={game} category={category} />
)

describe("ImageViewer", () => {
    it("should exist", () => {
        // Arrange
        const sut = imageViewer(defaultProps)!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        expect(wrapper).toHaveLength(1);
    });

    it("should have an image with correct src and alt", () => {
        // Arrange
        const game = 'platinum';
        const category = defaultProps.category;
        const sut = imageViewer({ game, category })!!;

        // Act
        const wrapper = shallow(sut);
        const image = wrapper.find('img');

        // Assert
        expect(image.prop('src')).toEqual(`/images/${game}/${category.key}.png`);
        expect(image.prop('alt')).toEqual(`Image of  ${category.key}`);
    });
});