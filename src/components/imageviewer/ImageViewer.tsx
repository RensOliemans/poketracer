import {FC} from "react";
import './ImageViewer.css';
import {Category} from "../../constants/locations";

export type ImageViewerProps = {
  game: string,
  category: Category;
}

const ImageViewer: FC<ImageViewerProps> = ({game, category}) => {
  const getImageUrl = (game: string, category: string) => {
    return `${process.env.PUBLIC_URL}/images/${game}/${category}.png`;
  }

  const getImageAlt = (category: string) => {
    return `Image of ${process.env.PUBLIC_URL} ${category}`;
  }

  return (
    <div className="image-viewer">
      <img
        src={getImageUrl(game, category.key)}
        alt={getImageAlt(category.key)}
        height="850"
        width="850"
      />
    </div>
  )
}

export default ImageViewer;
