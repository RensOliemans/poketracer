import Entrances, {EntrancesProps} from "./Entrances";
import React, {FC, RefObject} from "react";
import {mount, shallow} from "enzyme";
import Note from "../note/Note";
import ButtonGrid from "../buttongrid/ButtonGrid";
import {Category, Entrance, Location} from "../../constants/locations";
import Link from "../../data/link";
import {Block} from "../../data/block";

const focus: RefObject<HTMLInputElement> = React.createRef();

const defaultCategory = new Category('cat', 'Category')
const defaultLocation = new Location('loc', 'Location', defaultCategory);
const otherCategory = new Category('cat2', 'Category 2')
const otherLocation = new Location('loc2', 'Location 2', otherCategory);

const defaultProps = {
  entrance: null,
  entrances: [],
  setEntrance: jest.fn(),
  setCategory: jest.fn(),
  setRoute: jest.fn(),
  links: [],
  entrancesToHighlight: [],
  onEnter: jest.fn(),
  onLeave: jest.fn(),
  getEntranceFromKey: jest.fn(),
  focusRef: focus,
}

const entrance: FC<EntrancesProps> = ({
  entrance,
  entrances,
  setEntrance,
  setCategory,
  setRoute,
  links,
  entrancesToHighlight,
  onEnter,
  onLeave,
  getEntranceFromKey,
  focusRef = focus,
}) => (<Entrances
    entrance={entrance}
    entrances={entrances}
    setEntrance={setEntrance}
    setCategory={setCategory}
    setRoute={setRoute}
    links={links}
    entrancesToHighlight={entrancesToHighlight}
    onEnter={onEnter}
    onLeave={onLeave}
    getEntranceFromKey={getEntranceFromKey}
    focusRef={focusRef}
  />);

describe('Entrance', () => {
  it('should exist', () => {
    // Arrange
    const sut = entrance(defaultProps)!!

    // Act
    const wrapper = shallow(sut);

    // Assert
    expect(wrapper).toHaveLength(1);
  });

  it("should set correct selectionString when no entrance is selected", () => {
    // Arrange
    const sut = entrance(defaultProps)!!;

    // Act
    const wrapper = shallow(sut);

    // Assert
    expect(wrapper.find('p').text()).toEqual('Click on an entrance to link it.')
  });

  it("should set correct selectionString when entrance is selected", () => {
    // Arrange
    const ent = new Entrance('entr_a', 'Entrance A', defaultLocation);
    const sut = entrance({ ...defaultProps, entrance: ent })!!;

    // Act
    const wrapper = shallow(sut);

    // Assert
    expect(wrapper.find('p').text()).toEqual('Entrance A → ')
  });

  describe("Note", () => {
    it("should exist", () => {
      // Arrange
      const sut = entrance(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      expect(wrapper.find(Note)).toHaveLength(1);
    });

    it('should setEntrance with note when saving note',  () => {
      // Arrange
      const setEntrance = jest.fn();
      const sut = entrance({
        ...defaultProps,
        setEntrance,
      })!!;

      // Act - mount since we're doing stuff inside Note
      const wrapper = mount(sut);
      wrapper.find(Note).find('form').simulate('submit')

      // Assert
      expect(setEntrance).toHaveBeenCalledTimes(1);
      expect(setEntrance).toHaveBeenCalledWith(null, null, "")
    });

    it("should pass focusRef to note", () => {
      // Arrange
      const sut = entrance({ ...defaultProps, focusRef: focus })!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      expect(wrapper.find(Note).prop('focusRef')).toBe(focus);
    });
  });

  describe("ButtonGrid", () => {
    it('should exist', () => {
      // Arrange
      const sut = entrance(defaultProps)!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      expect(wrapper.find(ButtonGrid)).toHaveLength(1);
    });

    describe("Conversion of Entrances", () => {
      it('should convert entrances without links correctly', () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
          new Entrance('entr_b', 'Entrance B', defaultLocation),
        ];
        const sut = entrance({ ...defaultProps,entrances })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const ents = wrapper.find(ButtonGrid).prop('keyNamePairs');
        expect(ents).toEqual(entrances);
        expect(ents[0].modifiedName).toEqual('');
      });

      it('should set the modifiedName of entrances with links', () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
          new Entrance('entr_b', 'Entrance B', defaultLocation),
        ];
        const links = [
          new Link('entr_a', 'other_a', false, Block.NONE, ''),
        ];
        const getEntranceFromKey = jest.fn();
        const sut = entrance({
          ...defaultProps,
          entrances, links, getEntranceFromKey,
        })!!;

        getEntranceFromKey.mockReturnValue(new Entrance('other_a', 'Other A', defaultLocation));

        // Act
        const wrapper = shallow(sut);

        // Assert
        const ent = wrapper.find(ButtonGrid).prop('keyNamePairs')[0];
        expect(ent.key).toEqual('entr_a');
        expect(ent.modifiedName).toEqual('Entrance A → Other A')
        expect(ent.classNames).toEqual([]);
      });

      it('should set the className for entrance with block', () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
          new Entrance('entr_b', 'Entrance B', defaultLocation),
        ];
        const links = [
          new Link('entr_a', null, false, Block.WATERFALL, ''),
          new Link('entr_b', null, false, Block.DEAD_END, ''),
        ];
        const sut = entrance({ ...defaultProps, entrances, links, })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const ents = wrapper.find(ButtonGrid).prop('keyNamePairs');
        expect(ents[0].key).toEqual('entr_a');
        expect(ents[0].modifiedName).toEqual('')
        expect(ents[0].classNames).toEqual(["waterfall"]);
        expect(ents[1].key).toEqual('entr_b');
        expect(ents[1].modifiedName).toEqual('');
        expect(ents[1].classNames).toEqual(["dead_end"]);
      });

      it('should set the className and text for link with note', () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
        ];
        const links = [
          new Link('entr_a', null, false, Block.NONE, 'Note'),
        ];
        const sut = entrance({ ...defaultProps, entrances, links, })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const ent = wrapper.find(ButtonGrid).prop('keyNamePairs')[0];
        expect(ent.key).toEqual('entr_a');
        expect(ent.modifiedName).toEqual('Entrance A — Note')
        expect(ent.classNames).toEqual(["note"]);
      });
    });

    describe("Other props of ButtonGrid", () => {
      it("should add links to highlighted keys", () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
          new Entrance('entr_b', 'Entrance B', defaultLocation),
          new Entrance('entr_c', 'Entrance C', defaultLocation),
        ];
        const links = [
          new Link('entr_a', 'other_a', false, Block.NONE, ''),
          new Link('other_b', 'entr_b', false, Block.NONE, ''),
        ];
        const sut = entrance({ ...defaultProps, entrances, links, })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        const expected = ['entr_a', 'entr_b', 'other_a', 'other_b'].sort();
        expect(wrapper.find(ButtonGrid).prop('highlightedKeys').sort()).toEqual(expected);
      });

      it("should call setEntrance without block on ButtonGrid click", () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
        ];
        const setEntrance = jest.fn();
        const sut = entrance({
          ...defaultProps,
          entrances, setEntrance,
        })!!;

        // Act
        const wrapper = mount(sut);
        wrapper.find(ButtonGrid).find('button').simulate('click');

        // Assert
        expect(setEntrance).toHaveBeenCalledTimes(1);
        expect(setEntrance).toHaveBeenCalledWith(entrances[0], null, null);
      });

      it("should call setCategory on ctrlClick", () => {
        // Arrange
        const entrances = [
          new Entrance('entr_a', 'Entrance A', defaultLocation),
        ];
        const links = [new Link('entr_a', 'other_a', false, Block.NONE, '')];
        const setCategory = jest.fn();
        const getEntranceFromKey = jest.fn();
        getEntranceFromKey.mockReturnValue(new Entrance('other_a', 'Other A', otherLocation));
        const sut = entrance({
          ...defaultProps,
          entrances, links, setCategory, getEntranceFromKey,
        })!!;

        // Act
        const wrapper = mount(sut);
        wrapper.find(ButtonGrid).find('button').simulate('click', { ctrlKey: true });

        // Assert
        expect(setCategory).toHaveBeenCalledTimes(1);
        expect(setCategory).toHaveBeenCalledWith(otherCategory);
      });

      it("should pass setRoute, onEnter and onLeave to ButtonGrid", () => {
        // Arrange
        const setRoute = jest.fn();
        const onEnter = jest.fn();
        const onLeave = jest.fn();
        const sut = entrance({
          ...defaultProps,
          setRoute, onEnter, onLeave,
        })!!;

        // Act
        const wrapper = shallow(sut);

        // Assert
        expect(wrapper.find(ButtonGrid).prop('onShiftClick')).toBe(setRoute)
        expect(wrapper.find(ButtonGrid).prop('onEnter')).toBe(onEnter)
        expect(wrapper.find(ButtonGrid).prop('onLeave')).toBe(onLeave)
      });
    });
  });
});