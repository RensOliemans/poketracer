import './Entrances.css';
import {Category, Entrance, KeyName} from "../../constants/locations";
import React, {FC, RefObject, useState} from "react";
import ButtonGrid from "../buttongrid/ButtonGrid";
import Link from "../../data/link";
import {getEntrancesOfLinks, other} from "../../util";
import {Block} from "../../data/block";
import Note from "../note/Note";

export type EntrancesProps = {
  entrance: Entrance | null;
  entrances: Entrance[];
  setEntrance: (entrance: Entrance | null, block: Block | null, note: string | null) => void;
  setCategory: (category: Category) => void;
  setRoute: (entrance: Entrance) => void;
  links: Link[],
  entrancesToHighlight: string[],
  onEnter: (entrance: Entrance) => void,
  onLeave: () => void,
  getEntranceFromKey: (key: string) => Entrance | null,
  focusRef: RefObject<HTMLInputElement>;
}

const Entrances: FC<EntrancesProps> = ({
  entrance,
  entrances,
  setEntrance,
  setCategory,
  setRoute,
  links,
  entrancesToHighlight,
  onEnter,
  onLeave,
  getEntranceFromKey,
  focusRef,
}) => {
  const [note, setNote] = useState('');

  const getSelectionString = (entrance: Entrance | null) => {
    if (!entrance) {
      return 'Click on an entrance to link it.';
    } else {
      return `${entrance.name} → `;
    }
  }

  const getLink = (entrance: Entrance) => {
    const linkOfEntrance = links.filter(l => l.entrance === entrance.key || l.destination === entrance.key);
    if (linkOfEntrance.length === 0)
      return null;

    return linkOfEntrance[0];
  }

  const getOther = (entrance: Entrance) => {
    const link = getLink(entrance);
    if (!link)
      return
    const otherKey = other(link, entrance.key);
    if (!otherKey)
      return
    return getEntranceFromKey(otherKey);
  }

  const convertEntrances = () => {
    return entrances.map(e => {
      const link = getLink(e);
      if (!link) {
        e.modifiedName = '';
        e.classNames = [];
        return e;
      }
      const otherKey = other(link, e.key);
      if (link.block) {
        e.classNames = [Block[link.block].toLowerCase()];
        e.modifiedName = '';
      }
      else if (link.note) {
        e.classNames = ['note']
        e.modifiedName = e.name + ' — ' + link.note;
      }
      else if (otherKey) {
        const entrance = getEntranceFromKey(otherKey);
        e.classNames = [];
        e.modifiedName = `${e.name} → ${entrance?.name}`;
      }
      return e;
    })
  }

  const handleCtrlClick = (kn: KeyName) => {
    const otherKey = getOther(kn as Entrance);
    if (otherKey)
      setCategory((otherKey as Entrance).location.category);
  };

  const saveNote = (note: string) => {
    setEntrance(null, null, note)
  }

  return (
    <div className="entrances">
      <h2>Entrances</h2>
      <p>{getSelectionString(entrance)}</p>
      <Note
        note={note}
        setNote={setNote}
        saveNote={saveNote}
        focusRef={focusRef}
      />
      <ButtonGrid
        keyNamePairs={convertEntrances()}
        highlightedKeys={getEntrancesOfLinks(links)}
        linkedKeys={entrancesToHighlight}
        onClick={(e) => setEntrance(e, null, null)}
        onCtrlClick={handleCtrlClick}
        onShiftClick={setRoute}
        onEnter={onEnter}
        onLeave={onLeave}
      />
    </div>
  )
}

export default Entrances;
