import ButtonGrid, {ButtonGridProps} from "./ButtonGrid";
import {KeyName} from "../../constants/locations";
import {shallow} from "enzyme";
import {FC} from "react";


const defaultProps = {
  keyNamePairs: [],
  highlightedKeys: [],
  linkedKeys: [],
  onClick: jest.fn(),
  onCtrlClick: jest.fn(),
  onShiftClick: jest.fn(),
  onEnter: jest.fn(),
  onLeave: jest.fn(),
};

const buttonGrid: FC<ButtonGridProps> = ({
  keyNamePairs,
  highlightedKeys,
  linkedKeys,
  onClick,
  onCtrlClick,
  onShiftClick,
  onEnter,
  onLeave
}) => (
  <ButtonGrid
    keyNamePairs={keyNamePairs}
    highlightedKeys={highlightedKeys}
    linkedKeys={linkedKeys} 
    onClick={onClick}
    onCtrlClick={onCtrlClick}
    onShiftClick={onShiftClick}
    onEnter={onEnter}
    onLeave={onLeave}
  />)


describe("ButtonGrid", () => {
  describe("Layout", () => {
    it("should exist", () => {
      // Arrange
      const sut = buttonGrid(defaultProps)!!;

      // Act
      const wrapper = shallow(sut)

      // Assert
      expect(wrapper).toHaveLength(1);
    });
  });

  describe("Button Color", () => {
    it("should have no specific class for regular buttons", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const sut = buttonGrid({ ...defaultProps, keyNamePairs })!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const btn = wrapper.find("button");
      expect(btn.hasClass(/[a-z]+/)).toBe(false);
    });

    it("should have linked class if keyName appears in linkedKeys", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "Second"),
      ];
      const linkedKeys = ["second"];
      const sut = buttonGrid({ ...defaultProps, keyNamePairs, linkedKeys })!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const btns = wrapper.find("button");
      expect(btns.last().hasClass("linked")).toBe(true);
    });

    it("should have linked and highlighted class for correct keyNames", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "Second"),
        new KeyName("third", "Numero 3"),
      ];
      const linkedKeys = ["second"];
      const highlightedKeys = ["second", "third"];
      const sut = buttonGrid({ ...defaultProps,
        keyNamePairs, highlightedKeys, linkedKeys
      })!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const btns = wrapper.find("button");
      expect(btns.first().hasClass("linked")).toBe(false);
      expect(btns.first().hasClass("highlighted")).toBe(false);
      expect(btns.at(1).hasClass("linked")).toBe(true);
      expect(btns.at(1).hasClass("highlighted")).toBe(true);
      expect(btns.at(2).hasClass("linked")).toBe(false);
      expect(btns.at(2).hasClass("highlighted")).toBe(true);
    });

    it('should show classNames from keyNames', () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "Second"),
      ];
      keyNamePairs[0].classNames = ["dead_end"];
      keyNamePairs[1].classNames = ["trainer", "event"];
      const highlightedKeys = ["first"];
      const linkedKeys = ["second"];
      const sut = buttonGrid({ ...defaultProps, keyNamePairs, highlightedKeys, linkedKeys})!!;

      // Act
      const wrapper = shallow(sut);

      // Assert
      const btns = wrapper.find("button");
      expect(btns.first().hasClass("linked")).toBe(false);
      expect(btns.first().hasClass("highlighted")).toBe(true);
      expect(btns.first().hasClass("dead_end")).toBe(true);
      expect(btns.last().hasClass("linked")).toBe(true);
      expect(btns.last().hasClass("highlighted")).toBe(false);
      expect(btns.last().hasClass("trainer")).toBe(true);
      expect(btns.last().hasClass("event")).toBe(true);
    });
  })

  describe("Button Text", () => {
    it("should show a button with entrance name", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")]
      const sut = buttonGrid({ ...defaultProps, keyNamePairs })!!;

      // Act
      const wrapper = shallow(sut)

      // Assert
      const btn = wrapper.find("button")
      expect(btn).toHaveLength(1);
      expect(btn.prop("value")).toEqual(keyNamePairs[0].key);
      expect(btn.text()).toEqual(keyNamePairs[0].name);
    });

    it("should show multiple buttons with entrance names", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "2nd !"),
      ]
      const sut = buttonGrid({ ...defaultProps, keyNamePairs})!!;

      // Act
      const wrapper = shallow(sut)

      // Assert
      const btns = wrapper.find("button")
      expect(btns).toHaveLength(2);
      expect(btns.first().prop("value")).toEqual(keyNamePairs[0].key);
      expect(btns.last().text()).toEqual(keyNamePairs[1].name);
    });

    it("should show buttons with entrance and destination name", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
      ]
      keyNamePairs[0].modifiedName = "First → entranceInOtherCategory";
      const sut = buttonGrid({ ...defaultProps, keyNamePairs})!!;

      // Act
      const wrapper = shallow(sut)

      // Assert
      const btn = wrapper.find("button")
      expect(btn).toHaveLength(1);
      expect(btn.prop("value")).toEqual(keyNamePairs[0].key)
      expect(btn.text()).toEqual(keyNamePairs[0].modifiedName);
    });
  });

  describe("Click Behaviour", () => {
    it("should call onClick on KeyName click", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onClick = jest.fn();
      const sut = buttonGrid({ ...defaultProps, keyNamePairs, onClick })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").simulate("click", { ctrlKey: false });

      // Assert
      expect(onClick).toHaveBeenCalledTimes(1);
      expect(onClick).toHaveBeenCalledWith(keyNamePairs[0]);
    });

    it("should call onClick on correct KeyName click", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "Second"),
      ];
      const onClick = jest.fn();
      const sut = buttonGrid({ ...defaultProps, keyNamePairs, onClick })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").first().simulate("click", { ctrlKey: false });
      wrapper.find("button").last().simulate("click", { ctrlKey: false });

      // Assert
      expect(onClick).toHaveBeenCalledTimes(2);
      expect(onClick).toHaveBeenCalledWith(keyNamePairs[0]);
      expect(onClick).toHaveBeenLastCalledWith(keyNamePairs[1]);
    });

    it("should call onCtrlClick on KeyName ctrlClick", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onClick = jest.fn();
      const onCtrlClick = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onClick, onCtrlClick,
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").simulate("click", { ctrlKey: true });

      // Assert
      expect(onClick).toHaveBeenCalledTimes(0);
      expect(onCtrlClick).toHaveBeenCalledTimes(1);
      expect(onCtrlClick).toHaveBeenCalledWith(keyNamePairs[0]);
    });

    it("should call onShiftClick on KeyName shiftClick", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onClick = jest.fn();
      const onCtrlClick = jest.fn();
      const onShiftClick = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onClick, onCtrlClick, onShiftClick,
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").simulate("click", { ctrlKey: false, metaKey: false, shiftKey: true });

      // Assert
      expect(onClick).toHaveBeenCalledTimes(0);
      expect(onCtrlClick).toHaveBeenCalledTimes(0);
      expect(onShiftClick).toHaveBeenCalledTimes(1);
      expect(onShiftClick).toHaveBeenCalledWith(keyNamePairs[0]);
    });
  });

  describe("Hovering Behaviour", () => {
    it("should not call onEnter when not hovering over", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onEnter = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onEnter,
      })!!;

      // Act
      shallow(sut);

      // Assert
      expect(onEnter).toHaveBeenCalledTimes(0);
    });

    it("should call onEnter when hovering over", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onEnter = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onEnter,
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").simulate("mouseenter");

      // Assert
      expect(onEnter).toHaveBeenCalledTimes(1);
      expect(onEnter).toHaveBeenCalledWith(keyNamePairs[0]);
    });

    it("should call onEnter for correct keyNames when hovering over", () => {
      // Arrange
      const keyNamePairs = [
        new KeyName("first", "First"),
        new KeyName("second", "2nd !"),
      ];
      const onEnter = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onEnter,
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").last().simulate("mouseenter");
      wrapper.find("button").first().simulate("mouseenter");

      // Assert
      expect(onEnter).toHaveBeenCalledTimes(2);
      expect(onEnter).toHaveBeenCalledWith(keyNamePairs[1]);
      expect(onEnter).toHaveBeenLastCalledWith(keyNamePairs[0]);
    });

    it("should not call onLeave when not leaving over", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onLeave = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onLeave,
      })!!;

      // Act
      shallow(sut);

      // Assert
      expect(onLeave).toHaveBeenCalledTimes(0);
    });

    it("should call onLeave when moving away with mouse", () => {
      // Arrange
      const keyNamePairs = [new KeyName("first", "First")];
      const onLeave = jest.fn();
      const sut = buttonGrid({
        ...defaultProps,
        keyNamePairs, onLeave,
      })!!;

      // Act
      const wrapper = shallow(sut);
      wrapper.find("button").simulate("mouseleave");

      // Assert
      expect(onLeave).toHaveBeenCalledTimes(1);
      expect(onLeave).toHaveBeenCalledWith(keyNamePairs[0]);
    });
  });
})
