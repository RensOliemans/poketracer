import React, {FC} from "react";
import './ButtonGrid.css'
import {KeyName} from "../../constants/locations";

export type ButtonGridProps = {
  keyNamePairs: KeyName[],
  highlightedKeys: string[],
  linkedKeys: string[],
  onClick: (key: any) => void,
  onCtrlClick: (key: any) => void,
  onShiftClick: (key: any) => void,
  onEnter: (key: any) => void,
  onLeave: (key: any) => void,
}

const ButtonGrid: FC<ButtonGridProps> = ({
  keyNamePairs,
  highlightedKeys,
  linkedKeys,
  onClick,
  onCtrlClick,
  onShiftClick,
  onEnter,
  onLeave,
 }) => {

  const determineClassName = (classNames: string[], key: string) => {
    const newClassNames = [...classNames];
    if (linkedKeys.indexOf(key) > -1)
      newClassNames.push('linked')
    if (highlightedKeys.indexOf(key) > -1)
      newClassNames.push('highlighted')
    return newClassNames.join(' ');
  }

  const handleClick = (e: React.MouseEvent<HTMLButtonElement>, l: KeyName) => {
    if (e.ctrlKey || e.metaKey)
      return onCtrlClick(l);
    else if (e.shiftKey)
      return onShiftClick(l);

    return onClick(l);
  }

  return (
    <div className="button-grid">
      {
        keyNamePairs.map(l =>
          <button
            key={l.key}
            value={l.key}
            onClick={(e) => handleClick(e, l)}
            onMouseEnter={() => onEnter(l)}
            onMouseLeave={() => onLeave(l)}
            type="submit"
            className={determineClassName(l.classNames, l.key)}
          >
            {l.modifiedName || l.name}
          </button>
        )
      }
    </div>
  )
}

export default ButtonGrid;
