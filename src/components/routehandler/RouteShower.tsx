import {FC, useEffect, useState} from "react";
import {Category, Entrance} from "../../constants/locations";
import {findPaths} from "./RouteHandler";
import Link from "../../data/link";
import {getEntranceFromKey} from "../../util";
import './RouteShower.css';

export type RouteShowerProps = {
  categories: Category[];
  links: Link[];
  route: Entrance | null;
  setRoute: (entrance: Entrance | null) => void;
}

const RouteShower: FC<RouteShowerProps> = ({categories, links, route, setRoute}) => {
  const [previousRoute, setPreviousRoute] = useState<Entrance | null>(null);
  const [routeText, setRouteText] = useState('Shift-click an entrance to start routing.');

  useEffect(() => {
    if (!previousRoute && route) {
      setRouteText(`${route.name} → ...`);
      setRoute(null);
      setPreviousRoute(route);
      return
    }

    if (!previousRoute || !route) return

    const path = findPaths(previousRoute, route, categories, links)
    if (!path) {
      setRouteText('No route exists.');
      setRoute(null);
      setPreviousRoute(null);
      return
    }
    setRouteText(path.map(key => getEntranceFromKey(categories, key)?.name).join(' → '));
    setPreviousRoute(null);
    setRoute(null);
  }, [previousRoute, categories, links, route, setRoute]);

  return (
    <div className="header-item routing">
      <div className="routeHeader">
        <h2>Route</h2>
      </div>
      <div className="route">
        {routeText}
      </div>
    </div>
  )
}

export default RouteShower;
