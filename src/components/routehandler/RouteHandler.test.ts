import {Category, Entrance, Location} from "../../constants/locations";
import {findPaths} from "./RouteHandler";
import Link from "../../data/link";
import {Block} from "../../data/block";

export const categories = [
    new Category('cat0', 'Category 0'),
    new Category('cat1', 'Category 1'),
    new Category('cat2', 'Category 2'),
    new Category('cat3', 'Category 3'),
];

export const locations = [
    new Location('loc0', 'Location 0', categories[0]),
    new Location('loc1', 'Location 1', categories[1]),
    new Location('loc2', 'Location 2', categories[2]),
    new Location('loc3', 'Location 3', categories[3]),
    new Location('loc3.1', 'Location 3-1', categories[3]),
];
categories.forEach(c => {
    c.locations = locations.filter(l => l.category === c);
});
export const entrances = [
    new Entrance('ent0.0', 'Entrance 0-0', locations[0]),
    new Entrance('ent0.1', 'Entrance 0-1', locations[0]),
    new Entrance('ent1', 'Entrance 1', locations[1]),
    new Entrance('ent2', 'Entrance 2', locations[2]),
    new Entrance('ent3.0.0', 'Entrance 3-0-0', locations[3]),
    new Entrance('ent3.0.1', 'Entrance 3-0-1', locations[3]),
    new Entrance('ent3.1.0', 'Entrance 3-1-0', locations[4]),
    new Entrance('ent3.1.1', 'Entrance 3-1-1', locations[4]),
    new Entrance('ent3.1.2', 'Entrance 3-1-2', locations[4]),
];
locations.forEach(l => {
    l.entrances = entrances.filter(e => e.location === l);
});


describe("Route Handler", () => {
    describe("Without links", () => {
        it("should give null if no path exists", () => {
            // Arrange
            const start = entrances[0];
            const goal = entrances[2];

            // Act
            const path = findPaths(start, goal, categories, []);

            // Assert
            expect(path).toBe(null);
        });

        it("should give path if one path exists", () => {
            // Arrange
            const start = entrances[0];
            const goal = entrances[1];

            // Act
            const path = findPaths(start, goal, categories, []);

            // Assert
            expect(path).toEqual([start.key, goal.key]);
        });

        it("should give path if multiple paths exist", () => {
            // Arrange
            const start = entrances[6];
            const goal = entrances[7];

            // Act
            const path = findPaths(start, goal, categories, []);

            // Assert
            expect(path).toEqual([start.key, goal.key]);
        });
    });

    describe("With links", () => {
        it("should give path if path exist with single link", () => {
            // Arrange
            const start = entrances.find(e => e.key === 'ent0.0')!!;
            const goal = entrances.find(e => e.key === 'ent1')!!;
            const links = [
                new Link(start.key, goal.key, false, Block.NONE, ''),
            ];

            // Act
            const path = findPaths(start, goal, categories, links);

            // Assert
            expect(path).toEqual([start.key, goal.key]);
        });

        it("should give path if path exist with links in between", () => {
            // Arrange
            const start = entrances.find(e => e.key === 'ent0.0')!!;
            const inBetween = entrances.find(e => e.key === 'ent0.1')!!;
            const goal = entrances.find(e => e.key === 'ent1')!!;
            const links = [
                new Link(inBetween.key, goal.key, false, Block.NONE, ''),
            ];

            // Act
            const path = findPaths(start, goal, categories, links);

            // Assert
            expect(path).toEqual([start.key, inBetween.key, goal.key]);
        });

        it("should give path when path is complexer", () => {
            // Arrange
            const start = entrances.find(e => e.key === 'ent0.0')!!;
            const hop1 = entrances.find(e => e.key === 'ent0.1')!!;
            const hop2 = entrances.find(e => e.key === 'ent3.0.1')!!;
            const hop3 = entrances.find(e => e.key === 'ent3.0.0')!!;
            const hop4 = entrances.find(e => e.key === 'ent3.1.1')!!;
            const goal = entrances.find(e => e.key === 'ent3.1.0')!!;
            const links = [
                new Link(hop1.key, hop2.key, false, Block.NONE, ''),
                new Link(hop3.key, hop4.key, false, Block.NONE, ''),
            ];

            // Act
            const path = findPaths(start, goal, categories, links);

            // Assert
            expect(path).toEqual([start.key, hop1.key, hop2.key, hop3.key, hop4.key, goal.key]);
        });

        it("should give shortest paths when multiple exist", () => {
            // Arrange
            const start = entrances.find(e => e.key === 'ent0.0')!!;
            const hop1 = entrances.find(e => e.key === 'ent0.1')!!;
            const hop2 = entrances.find(e => e.key === 'ent3.0.1')!!;
            const hop3 = entrances.find(e => e.key === 'ent3.0.0')!!;
            const hop4 = entrances.find(e => e.key === 'ent3.1.1')!!;
            const otherHop1 = entrances.find(e => e.key === 'ent3.1.2')!!;
            const goal = entrances.find(e => e.key === 'ent3.1.0')!!;
            const links = [
                new Link(hop1.key, hop2.key, false, Block.NONE, ''),
                new Link(hop3.key, hop4.key, false, Block.NONE, ''),
                new Link(start.key, otherHop1.key, false, Block.NONE, ''),
            ];

            // Act
            const path = findPaths(start, goal, categories, links);

            // Assert
            expect(path).toEqual([start.key, otherHop1.key, goal.key]);
        });
    });
});