import {Category, Entrance} from "../../constants/locations";
import Link from "../../data/link";
import {other} from "../../util";
import Graph from "../../graph";

export const findPaths = (start: Entrance, goal: Entrance, categories: Category[], links: Link[], n: number = 1) => {
  const graph = createGraph(categories, links);
  return graph.findShortestPath(start.key, goal.key);
};

const createGraph = (categories: Category[], links: Link[]) => {
  const edges = addEdges(categories, links);
  return new Graph(edges);
};

const addEdges = (categories: Category[], links: Link[]) => {
  const map = {};
  categories.forEach(c => c.locations.forEach(l => l.entrances.forEach(e => {
    const otherEntrances = l.entrances.filter(ent => e !== ent).map(ent => ent.key);
    const linksOfEntrance = links.filter(l => l.entrance === e.key || (l.destination && l.destination === e.key))
      .map(l => other(l, e.key)).filter(l => l != null) as string[];

    const result = {};
    otherEntrances.concat(linksOfEntrance).forEach(d => {
      // @ts-ignore
      result[d] = 1;
    })
    // @ts-ignore
    map[e.key] = result
  })));
  return map;
};

