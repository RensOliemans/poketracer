import {categories, entrances} from "./RouteHandler.test";
import {FC} from "react";
import RouteShower, {RouteShowerProps} from "./RouteShower";
import {mount, shallow} from "enzyme";
import {Entrance} from "../../constants/locations";

const defaultProps = {
    categories,
    links: [],
    route: null,
    setRoute: jest.fn()
};

const routeShower: FC<RouteShowerProps> = ({
    categories, links, route, setRoute
}) => (
    <RouteShower
        categories={categories}
        links={links}
        route={route}
        setRoute={setRoute}
    />);

describe("RouteShower", () => {
    describe("Without selected entrance", () => {
        it("should show instructions", () => {
            // Arrange
            const sut = routeShower(defaultProps)!!;

            // Act
            const wrapper = shallow(sut);

            // Assert
            expect(wrapper.find('.route').text()).toEqual('Shift-click an entrance to start routing.')
        });
    });

    describe("With selected entrance", () => {
        it("should show → ... with one entrance", () => {
            // I'm not confident in the current showRoute implementation (with useEffect and tracking the previousState)
            // This implementation is also difficult to test, this test is virtually impossible to do.
        });
    });
});