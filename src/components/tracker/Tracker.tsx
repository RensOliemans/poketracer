import LocationSelect from "../locationselect/LocationSelect";
import {Category, Entrance} from "../../constants/locations";
import React, {FC, RefObject, useEffect, useState} from "react";
import ImageViewer from "../imageviewer/ImageViewer";
import './Tracker.css';
import Link from "../../data/link";
import RouteShower from "../routehandler/RouteShower";
import {getEntranceFromKey, getCategoryFromKey} from "../../util";
import {Block} from "../../data/block";


type TrackerProps = {
  game: string
  categories: Category[];
  defaultCategory: Category;
  links: Link[];
  entrance: Entrance | null;
  setEntrance: (entrance: Entrance | null, block: Block | null, note: string | null) => void;
  focusRef: RefObject<HTMLInputElement>;
}

const Tracker: FC<TrackerProps> = ({
  game,
  categories,
  defaultCategory,
  links,
  entrance,
  setEntrance,
  focusRef,
}) => {
  const [category, setCategory] = useState<Category | null>(null);
  const [route, setRoute] = useState<Entrance | null>(null);

  useEffect(() => {
    if (!category) {
      const storedCat = localStorage.getItem('category');
      setCategory(storedCat ? getCategoryFromKey(categories, storedCat) : defaultCategory);
    } else {
      localStorage.setItem('category', category.key);
    }
  }, [category, categories, defaultCategory]);

  const linksOfCategory = () => {
    return links.filter(l => {
      const entrance = getEntranceFromKey(categories, l.entrance);
      const destination = l.destination && getEntranceFromKey(categories, l.destination);
      return (entrance && entrance.location.category === category) ||
        (destination && destination.location.category === category)
    });
  };

  return (category &&
    <div className="tracker">
      <LocationSelect
        categories={categories}
        category={category}
        setCategory={setCategory}
        entrance={entrance}
        setEntrance={setEntrance}
        setRoute={setRoute}
        links={linksOfCategory()}
        focusRef={focusRef}
      />
      <RouteShower
        route={route}
        setRoute={setRoute}
        categories={categories}
        links={links}
      />
      <ImageViewer game={game} category={category} />
    </div>
  )
}

export default Tracker;
