import React from 'react';
import './App.css';
import {HotKeys} from "react-hotkeys";
import Main from "./components/main/Main";

function App() {

  const keyMap = {
    CANCEL: ['escape', 'q'],
    DEAD_END: ['d'],
    CUT: ['c'],
    ROCK_SMASH: ['r'],
    STRENGTH: ['s'],
    SURF: ['f'],
    WATERFALL: ['w'],
    ROCK_CLIMB: ['b'],
    TRAINER: ['t'],
    EVENT: ['e'],
    NOTE: ['`'],
  }

  return (
    <HotKeys keyMap={keyMap} allowChanges={true}>
      <div className="app">
        <Main />
      </div>
    </HotKeys>
  );
}

export default App;
