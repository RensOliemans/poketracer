import {Block} from "./block";


class Link {
  id?: number;
  entrance: string;
  destination: string | null;
  one_way: boolean;
  block: Block;
  note: string;


  constructor(entrance: string, destination: string | null, one_way: boolean, block: Block, note: string) {
    this.entrance = entrance;
    this.destination = destination;
    this.one_way = one_way;
    this.block = block;
    this.note = note;
  }
}

export default Link;
