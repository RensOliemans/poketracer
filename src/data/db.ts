import Dexie, { Table } from 'dexie';
import Link from './link';
import {Block} from "./block";

export class LinkDB extends Dexie {
  links!: Table<Link, number>;
  constructor() {
    super('pokerandom');
    this.version(1).stores({
      links: '++id, entrance, destination, one_way, block, note'
    });
  }

  deleteByEntrance(entrance: string | null) {
    if (!entrance)
      return this.transaction('r?', this.links, () => {});

    return this.transaction('rw', this.links, async () => {
      await this.links.where("entrance").equals(entrance).delete();
      await this.links.where("destination").equals(entrance).delete();
    })
  }
}

export const db = new LinkDB();


export const addLink = (entrance: string, destination: string | null, one_way: boolean, block: Block, note: string) => {
  return db.deleteByEntrance(entrance).then(() => db.deleteByEntrance(destination)).then(
    () => db.links.add({ entrance, destination, one_way, block, note })
  );
}

export const addLinks = (links: Link[]) => {
  return db.links.bulkAdd(links);
}

