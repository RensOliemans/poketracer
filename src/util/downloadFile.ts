export const downloadFile = (data: any[], filename: string) => {
  const blob = new Blob(data, { type: 'text/html' });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = filename;
  link.click();
};
