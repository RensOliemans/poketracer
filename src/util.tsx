import {Category, KeyName} from "./constants/locations";
import Link from "./data/link";

export const getEntranceFromKey = (categories: Category[], key: string | null) => {
  for (const category of categories) {
    for (const location of category.locations) {
      const entrances = location.entrances.filter(e => e.key === key);
      if (entrances.length > 0)
        return entrances[0];
    }
  }
  return null;
}

export const getCategoryFromKey = (categories: Category[], key: string) => {
  for (const item of categories) {
    if (item.key === key) {
      return item;
    }
  }

  return null;
}

export const other = (link: Link, key: string) => {
  return link.entrance === key ? link.destination : link.entrance;
}


export const entrancesOfCategory = (categories: Category[], item: KeyName) => {
  const category = categories.filter(c => c.key === item.key)
  if (category.length !== 1) {
    console.error(`There should be exactly 1 category with key ${category}`)
  }

  return category[0].locations.flatMap(l => l.entrances);
}

export const getEntrancesOfLinks = (links: Link[]) => {
  // ternary operator below because TypeScript can't recognize the non-nullability of destination after the filter
  return links.filter(l => l.destination).flatMap(l => l.destination ? [l.entrance, l.destination] : [l.entrance]);
};
