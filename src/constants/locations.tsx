export class KeyName {
  key: string;
  name: string;
  modifiedName: string = '';
  classNames: string[] = [];

  constructor(key: string, name: string) {
    this.key = key;
    this.name = name;
  }
}


export class Category extends KeyName {
  locations: Location[] = [];
}


export class Location extends KeyName {
  category: Category;
  entrances: Entrance[] = [];

  constructor(key: string, name: string, category: Category) {
    super(key, name);
    this.category = category;
  }
}


export class Entrance extends KeyName {
  location: Location;

  constructor(key: string, name: string, location: Location) {
    super(key, name);
    this.location = location;
  }
}
